use serde::{Deserialize, Serialize};

/// This struct symbols a single device in a room
/// Rooms will be represented in a HashMap with the name of the room and another HashMap which
/// consists of device names and this struct as the device
#[derive(Clone, Deserialize, Serialize)]
pub struct Device {
    /// The name of the application, aka. client
    pub application: String,

    /// The channel on which the device runs.
    /// The channel is meant to be formatted as an URL. This URL is meant to contain the protocol,
    /// that the physical device speaks as the scheme, the GPIO pin number of the Raspberry PI or
    /// Arduino on which the physical device is connected as path and optional information for the
    /// application as the query part.
    ///
    /// # Example
    ///
    /// The URL for an **LED strip** with the **WS 2811** chipset, which is connected to the **pin 18**
    /// and the **mode BGR** may look like this:
    ///
    /// `ws2811:///18?mode=bgr`
    ///
    /// and another **i2c** device on GPIO **pin 13** will look like the following one
    ///
    /// `ic2:///13`
    pub channel: String,
}
