use std::collections::HashMap;
use std::fs::{File, OpenOptions};

use crate::room::device::Device;

/// This trait says you, how you can control rooms.
pub trait RoomManager {
    /// This method returns the name of all registered rooms
    /// If there are no rooms registered, it will return an empty vector
    fn get_rooms(&self) -> Vec<String>;

    /// This method will add a new room, if not another room is already named the same
    /// If the room exists, the result will be an error with an explaining message
    fn add_room(&mut self, room: String) -> Result<(), String>;

    /// This method will remove a room, if it was added before
    /// If the room does not already exists, the result will be an error with an explaining message
    fn remove_room(&mut self, room: &String) -> Result<(), String>;

    /// This method returns all registered devices for the given room
    /// If the room does not exists, the result will be an error with an explaining message
    fn get_room_devices(&self, room: &String) -> Result<HashMap<String, Device>, String>;

    /// This method adds a new device to a given room
    /// If the device already exists, an error will be outputted through the Result
    /// If the room does not exist, an error will be outputted as well.
    fn add_room_device(&mut self, room: String, device_name: String, device: Device) -> Result<(), String>;

    /// This method removes a device from a room
    /// If the device does not exists, an error will be outputted through the Result
    /// If the room does not exist, an error will be outputted as well.
    fn remove_room_device(&mut self, room: &String, device_name: &String) -> Result<(), String>;
}

pub struct RoomManagerImpl {
    file_name: Option<String>,
    rooms: HashMap<String, HashMap<String, Device>>,
}

impl RoomManagerImpl {
    pub fn new(file_name: Option<String>) -> Self {
        let mut manager = Self {
            file_name,
            rooms: HashMap::new(),
        };

        manager.read_yaml();

        manager
    }

    fn open_self_file(&self) -> Option<File> {
        if self.file_name.is_none() {
            return None;
        }

        let file_result = OpenOptions::new().write(true).read(true).create(true).open(self.file_name.as_ref().unwrap());

        if file_result.is_err() {
            println!("Could not open file {} for write due to an error: {:?}", self.file_name.as_ref().unwrap(), file_result.err().unwrap());

            return None;
        }

        Some(file_result.unwrap())
    }

    fn write_yaml(&self) {
        if self.file_name.is_none() {
            return;
        }

        let file = self.open_self_file();

        if file.is_none() {
            return;
        }

        let file = file.unwrap();
        let write_result = serde_yaml::to_writer(file, &self.rooms);

        if write_result.is_err() {
            println!("Could not write to file due to an error: {:?}", write_result.err().unwrap());
        }
    }

    fn read_yaml(&mut self) {
        if self.file_name.is_none() {
            return;
        }

        let file = self.open_self_file();

        if file.is_none() {
            return;
        }

        let from_reader_result = serde_yaml::from_reader(file.unwrap());

        if from_reader_result.is_err() {
            println!("Could not read rooms from file due to an error: {:?}", from_reader_result.err().unwrap());

            return;
        }

        let new_rooms: HashMap<String, HashMap<String, Device>> = from_reader_result.unwrap();
        self.rooms = new_rooms;
    }
}

impl RoomManager for RoomManagerImpl {
    fn get_rooms(&self) -> Vec<String> {
        let mut rooms = vec![];

        for key in self.rooms.keys() {
            rooms.push(key.clone());
        }

        rooms
    }

    fn add_room(&mut self, room: String) -> Result<(), String> {
        if self.rooms.contains_key(&room) {
            return Err(String::from(format!("Room {} is already defined", &room)));
        }

        self.rooms.insert(room, HashMap::new());
        self.write_yaml();

        Ok(())
    }

    fn remove_room(&mut self, room: &String) -> Result<(), String> {
        if !self.rooms.contains_key(room) {
            return Err(String::from(format!("Room {} is not defined", room)));
        }

        self.rooms.remove(room);
        self.write_yaml();

        Ok(())
    }

    fn get_room_devices(&self, room: &String) -> Result<HashMap<String, Device>, String> {
        if !self.rooms.contains_key(room) {
            return Err(String::from(format!("Room {} does not exist", room)));
        }

        Ok(self.rooms.get(room).unwrap().clone())
    }

    fn add_room_device(&mut self, room: String, device_name: String, device: Device) -> Result<(), String> {
        if !self.rooms.contains_key(&room) {
            return Err(String::from(format!("Room {} is not defined", &room)));
        }

        let devices = self.rooms.get_mut(&room).unwrap();

        if devices.contains_key(&device_name) {
            return Err(String::from(format!("Device {} for room {} is already defined", &device_name, &room)));
        }

        devices.insert(device_name, device);
        self.write_yaml();

        Ok(())
    }

    fn remove_room_device(&mut self, room: &String, device_name: &String) -> Result<(), String> {
        if !self.rooms.contains_key(room) {
            return Err(String::from(format!("Room {} is not defined", room)));
        }

        let devices = self.rooms.get_mut(room).unwrap();

        if !devices.contains_key(device_name) {
            return Err(String::from(format!("Device {} for room {} is not defined", &device_name, &room)));
        }

        devices.remove(device_name);
        self.write_yaml();

        Ok(())
    }
}
