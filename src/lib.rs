#![feature(fn_traits)]
#![feature(unboxed_closures)]

extern crate regex;
#[macro_use]
extern crate rouille;
extern crate serde;
#[macro_use]
extern crate serde_json;

pub mod communication {
    pub mod method_result;
    pub mod request;
    pub mod response_formatter;
}

pub mod config {
    pub mod application_config;
}

pub mod connection {
    pub mod client;
    pub mod client_manager;
    pub mod connection_factory;
}

pub mod http {
    pub mod client_request_handler;
    pub mod request_handler;
    pub mod request_structures;
    pub mod room_request_handler;
    pub mod server_wrapper;
}

pub mod net {
    pub mod tcp_reader;
    pub mod tcp_writer;
}

pub mod room {
    pub mod room_manager;
    pub mod device;
}

pub mod util {
    pub mod script_preprocessor;
}
