#![feature(fn_traits)]
#![feature(unboxed_closures)]

extern crate regex;
#[macro_use]
extern crate rouille;
extern crate serde;
#[macro_use]
extern crate serde_json;

use crate::config::application_config::ApplicationConfig;
use crate::http::server_wrapper::{ServerWrapper, ServerWrapperImpl};

pub mod connection;
pub mod net;
pub mod communication;
pub mod http;
pub mod config;
pub mod room;
pub mod util;

fn main() {
    let config = ApplicationConfig::new(String::from("config.yaml"));

    ServerWrapperImpl::new(config.server_address.clone(), config.server_port).start();
}
