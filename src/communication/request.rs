use serde::{Deserialize, Serialize};
use serde_json::Value;

/// Contains the structure for a method request
/// Contains the name of the method that should be called
/// And a list of arguments which is a `serde_json::Value`
#[derive(Serialize, Deserialize)]
pub struct RequestData {
    /// The name of the method that is called
    pub method_name: String,

    /// The list of arguments which are supplied to the method
    pub arguments: Value,
}
