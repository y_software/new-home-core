use serde_json::{Map, Value};

/// Implements the response formatter for json values
pub struct JsonResponseFormatter {
    response: Vec<Value>,
}

/// The Response formatter is meant to create a `Value` (mainly) for the `MethodResponse` struct
/// It collects all possible response data types
/// These types are meant for displaying by other applications
pub trait ResponseFormatter {
    /// Clears the response buffer
    fn clear(&mut self);

    /// Gets the response buffer as an `serde_json::Value`
    fn get_value(&self) -> Value;

    /// Adds an error formatted section to the internal buffer
    fn add_error(&mut self, message: &String);

    /// Adds an info formatted section to the internal buffer
    fn add_info(&mut self, message: &String);

    /// Adds a debug formatted section to the internal buffer
    fn add_debug(&mut self, message: &String);

    /// Adds a success formatted section to the internal buffer
    fn add_success(&mut self, message: &String);

    /// Adds a markdown formatted section to the internal buffer
    fn add_markdown(&mut self, message: &String);

    /// Adds a list formatted section to the internal buffer
    fn add_list(&mut self, items: Vec<Value>);

    /// Adds a table formatted section to the internal buffer
    fn add_table(&mut self, items: Map<String, Value>);
}

impl JsonResponseFormatter {
    /// Creates a new JSON formatter with an empty buffer
    pub fn new() -> Self {
        Self {
            response: vec![]
        }
    }

    /// Creates a basic `Value` which contains the content with the error
    pub fn error(message: &String) -> Value {
        let mut formatter = Self::new();
        formatter.add_error(message);

        formatter.as_response(false)
    }

    /// Creates a basic `Value` which contains the content with the error
    pub fn success(message: &String) -> Value {
        let mut formatter = Self::new();
        formatter.add_success(message);

        formatter.as_response(true)
    }

    /// Creates a basic `Value` which contains the wrapped items object
    pub fn table(items: Map<String, Value>) -> Value {
        let mut formatter = Self::new();
        formatter.add_table(items);

        formatter.as_response(true)
    }

    /// Returns the buffer as a `Value` with the given code
    pub fn as_response(&self, success: bool) -> Value {
        json!({
            "success": success,
            "message": self.get_value(),
        })
    }

    fn get_named_value(&self, name: &str, value: &Value) -> Value {
        let mut map = Map::new();

        map.insert(String::from(name.clone()), value.clone());

        Value::from(map)
    }
}

impl ResponseFormatter for JsonResponseFormatter {
    fn clear(&mut self) {
        self.response.clear();
    }

    fn get_value(&self) -> Value {
        Value::from(self.response.clone())
    }

    fn add_error(&mut self, message: &String) {
        self.response.push(self.get_named_value("error", &Value::from(message.clone())));
    }

    fn add_info(&mut self, message: &String) {
        self.response.push(self.get_named_value("info", &Value::from(message.clone())));
    }

    fn add_debug(&mut self, message: &String) {
        self.response.push(self.get_named_value("debug", &Value::from(message.clone())));
    }

    fn add_success(&mut self, message: &String) {
        self.response.push(self.get_named_value("success", &Value::from(message.clone())));
    }

    fn add_markdown(&mut self, message: &String) {
        self.response.push(self.get_named_value("markdown", &Value::from(message.clone())));
    }

    fn add_list(&mut self, items: Vec<Value>) {
        self.response.push(self.get_named_value(
            "list",
            &Value::from(items),
        ))
    }

    fn add_table(&mut self, items: Map<String, Value>) {
        self.response.push(self.get_named_value(
            "table",
            &Value::from(items),
        ))
    }
}
