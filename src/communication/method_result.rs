use serde::{Deserialize, Serialize};
use serde_json::Value;

/// This struct contains the structure for a method return
/// It consists of a return code and a single message which can be an error, a success or an info message
/// Is used together with `serde` for serialization and deserialization to and from JSON (mainly)
#[derive(Clone, Deserialize, Serialize)]
pub struct MethodResult {
    /// The return code of the method
    /// 0 = success
    /// > 0 = error
    pub code: i8,

    /// The message of the method
    /// Can be empty if there is nothing to say about the actions that were done
    pub message: Option<Value>,
}
