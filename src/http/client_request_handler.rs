use std::borrow::Cow;
use std::sync::{Arc, RwLock};

use regex::Regex;
use rouille::{Request, Response};
use serde_json::Value;

use crate::communication::method_result::MethodResult;
use crate::communication::response_formatter::JsonResponseFormatter;
use crate::connection::client_manager::ClientManager;
use crate::http::request_handler::RequestHandler;
use crate::http::request_structures::ClientPutBody;
use crate::util::script_preprocessor::{ScriptPreprocessor, ScriptVariables};

/// Implements request handler for client type actions
/// Uses the `ClientManager` trait for communicating with the home clients
pub struct ClientRequestHandler {
    client_manager: Arc<RwLock<dyn ClientManager>>
}

impl ClientRequestHandler {
    pub fn new(client_manager: Arc<RwLock<dyn ClientManager>>) -> Self {
        Self { client_manager }
    }

    /// Handles the GET method for the `/client` route.
    /// Returns an array of client names in the json key `data`
    /// Provides a `success` key as well
    pub fn get_clients(&self) -> Response {
        let client_names = self.client_manager.read().unwrap().get_registered_clients();

        Response::json(&json!({
            "list": client_names,
            "success": true
        }))
    }

    /// Handles the PUT method for the `/client/{client_name}` url with the client name.
    /// Can return a `message` key for the output.
    /// Has to return a `success` key for the output.
    pub fn put_client(&self, name: String, body: ClientPutBody) -> Response {
        self.client_manager.write().unwrap().register_client(
            body.address,
            name,
        );

        Response::json(&JsonResponseFormatter::success(&String::from("Client added!")))
    }

    /// Handles the DELETE method for the `/client/{client_name}` path.
    /// Returns an error if the client could not be removed
    /// Returns success if it was able to do so
    pub fn remove_client(&self, client_name: String) -> Response {
        let result = self.client_manager.write().unwrap().remove_client(&client_name);

        if result.is_err() {
            return Response::json(&JsonResponseFormatter::error(&result.err().unwrap()));
        }

        Response::json(&json!({
            "success": true
        }))
    }

    /// Forwards the given arguments to the given client and calls the given method
    /// Will return the `response` from the client
    /// Has to provide a `success` key to the output
    pub fn call_client_method(
        &self,
        client_name: String,
        method_name: String,
        arguments: Value,
    ) -> Response {
        let client_manager = self.client_manager.read().unwrap();
        let client = client_manager.get_client(&client_name);

        if client.is_none() {
            return Response::json(
                &JsonResponseFormatter::error(&String::from("Client not found"))
            );
        }

        let method_result = client.unwrap().call_method(&method_name, arguments);

        Response::json(&json!({
            "response": &method_result,
            "success": method_result.code == 0
        }))
    }

    pub fn get_client_script(&self, request: &Request) -> Response {
        let regex = Regex::new(r#"/client/([^/]+)/script(.*)"#).unwrap();
        let url = request.url();
        if !regex.is_match(&url) {
            return Response::empty_404();
        }

        let captures = regex.captures(&url).unwrap();
        if captures.len() < 2 {
            return Response::empty_404();
        }

        let client_name = match captures.get(1) {
            None => String::new(),
            Some(client_name) => String::from(client_name.as_str())
        };
        let script_name = match captures.get(2) {
            None => String::new(),
            Some(script_name) => String::from(script_name.as_str())
        };

        let method_result = {
            let manager = self.client_manager.read().unwrap();

            match manager.get_client(&client_name) {
                Some(client) => client.call_method(
                    &String::from("get_script"),
                    json!({
                        "script_name": &script_name
                    }),
                ),
                None => {
                    println!("none");
                    MethodResult { code: 1, message: Some(json!({"error": "Could not get client!"})) }
                }
            }
        };

        if method_result.code != 0 || method_result.message.is_none() {
            return Response::empty_404();
        }

        let message = method_result.message.unwrap();
        let script = match message.get("script") {
            None => { return Response::empty_404(); }
            Some(val) => match val.as_str() {
                None => { return Response::empty_404(); }
                Some(script) => String::from(script)
            }
        };

        let mut response = Response::text(ScriptPreprocessor::new(script).process(ScriptVariables {
            app: format!("/client/{}/script/", &client_name),
            framework: self.get_origin(request),
        }));

        response.headers.clear();

        response.headers.push((
            Cow::from(String::from("Content-Type")),
            Cow::from(String::from("application/javascript")),
        ));

        response
    }

    fn get_origin(&self, request: &Request) -> String {
        let origin = match request.header("Origin") {
            None => match request.header("Referer") {
                None => String::new(),
                Some(referer) => String::from(referer)
            }
            Some(origin) => String::from(origin)
        };

        origin
    }
}

impl RequestHandler for ClientRequestHandler {
    fn handle_request(&self, request: &Request) -> Response {
        router!(
            request,
            (GET) (/client) => {
                self.get_clients()
            },
            (PUT) (/client/{name: String}) => {
                let request_body: ClientPutBody = try_or_400!(rouille::input::json_input(request));

                self.put_client(name, request_body)
            },
            (DELETE) (/client/{name: String}) => {
                self.remove_client(name)
            },
            (CALL) (/client/{client_name: String}/method/{method_name: String}) => {
                let arguments: Value = try_or_400!(rouille::input::json_input(request));

                self.call_client_method(client_name, method_name, arguments)
            },
            (HELP) (/client/{client_name: String}/method/{method_name: String}) => {
                self.call_client_method(client_name, String::from("help"), json!({"method": method_name}))
            },
            _ => {
                self.get_client_script(request)
            }
        )
    }
}

unsafe impl Send for ClientRequestHandler {}

unsafe impl Sync for ClientRequestHandler {}
