use std::sync::{Arc, RwLock};

use rouille::{Request, Response};

use crate::communication::response_formatter::JsonResponseFormatter;
use crate::http::request_handler::RequestHandler;
use crate::room::device::Device;
use crate::room::room_manager::RoomManager;

/// Supplies the implementation for the request handler
/// Uses the `RoomManager` trait for creating rooms and properties/devices
pub struct RoomRequestHandler {
    room_manager: Arc<RwLock<dyn RoomManager>>,
}

impl RoomRequestHandler {
    pub fn new(room_manager: Arc<RwLock<dyn RoomManager>>) -> Self {
        Self { room_manager }
    }

    pub fn get_rooms(&self) -> Response {
        let rooms = self.room_manager.read().unwrap().get_rooms();

        Response::json(&json!({
                    "success": true,
                    "list": rooms
        }))
    }

    pub fn add_room(&self, name: String) -> Response {
        let result = self.room_manager.write().unwrap().add_room(name);

        if result.is_err() {
            return Response::json(&JsonResponseFormatter::error(&result.err().unwrap()));
        }

        Response::json(&json!({
            "success": true
        }))
    }

    pub fn remove_room(&self, name: String) -> Response {
        let result = self.room_manager.write().unwrap().remove_room(&name);

        if result.is_err() {
            return Response::json(&JsonResponseFormatter::error(&result.err().unwrap()));
        }

        Response::json(&json!({
            "success": true
        }))
    }

    pub fn get_room_devices(&self, name: String) -> Response {
        let result = self.room_manager.write().unwrap().get_room_devices(&name);

        if result.is_err() {
            return Response::json(&JsonResponseFormatter::error(&result.err().unwrap()));
        }

        Response::json(&json!({
            "success": true,
            "table": result.unwrap()
        }))
    }

    pub fn put_room_device(&self, room_name: String, device_name: String, device: Device) -> Response {
        let result = self.room_manager.write().unwrap().add_room_device(room_name, device_name, device);

        if result.is_err() {
            return Response::json(&JsonResponseFormatter::error(&result.err().unwrap()));
        }

        Response::json(&json!({
            "success": true
        }))
    }

    pub fn remove_room_device(&self, room_name: String, device_name: String) -> Response {
        let result = self.room_manager.write().unwrap().remove_room_device(&room_name, &device_name);

        if result.is_err() {
            return Response::json(&JsonResponseFormatter::error(&result.err().unwrap()));
        }

        Response::json(&json!({
            "success": true
        }))
    }
}

impl RequestHandler for RoomRequestHandler {
    fn handle_request(&self, request: &Request) -> Response {
        router!(
            request,
            (GET) (/room) => {
                self.get_rooms()
            },
            (PUT) (/room/{name: String}) => {
                self.add_room(name)
            },
            (DELETE) (/room/{name: String}) => {
                self.remove_room(name)
            },
            (GET) (/room/{room: String}/device) => {
                self.get_room_devices(room)
            },
            (PUT) (/room/{room: String}/device/{device_name: String}) => {
                let device: Device = try_or_400!(rouille::input::json_input(request));

                self.put_room_device(room, device_name, device)
            },
            (DELETE) (/room/{room: String}/device/{device_name: String}) => {
                self.remove_room_device(room, device_name)
            },
            _ => Response::empty_404()
        )
    }
}

unsafe impl Send for RoomRequestHandler {}

unsafe impl Sync for RoomRequestHandler {}
