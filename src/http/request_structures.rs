use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct ClientPutBody {
    pub address: String,
}
