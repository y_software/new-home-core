use std::sync::{Arc, RwLock};

use crate::connection::client_manager::ClientManagerImpl;
use crate::http::client_request_handler::ClientRequestHandler;
use crate::http::request_handler::RequestMultiHandler;
use crate::http::room_request_handler::RoomRequestHandler;
use crate::room::room_manager::RoomManagerImpl;

/// The replaceable interface for the ServerWrapper
pub trait ServerWrapper {
    /// Starts the web server
    /// Binds to a given endpoint (e.g. TCP Socket or Unix Socket)
    fn start(&self);
}

/// Implements the ServerWrapper with the `rouille` web framework/server
/// Wraps the `rouille::start_server` function in the interface and supplies a `RequestHandler`
pub struct ServerWrapperImpl {
    server: String,
    port: u16,
}

impl ServerWrapperImpl {
    /// Initializes the struct fields
    /// Does not initializes the server yet
    pub fn new(server: String, port: u16) -> Self {
        Self {
            server,
            port,
        }
    }
}

impl ServerWrapper for ServerWrapperImpl {
    /// initializes and starts the `rouille` web server
    fn start(&self) {
        let client_manager = Arc::new(RwLock::new(ClientManagerImpl::new()));
        let client_request_handler = ClientRequestHandler::new(client_manager);

        let room_manager = Arc::new(RwLock::new(RoomManagerImpl::new(
            Some(String::from("rooms.yaml"))
        )));
        let room_request_handler = RoomRequestHandler::new(room_manager);
        let request_multi_handler = RequestMultiHandler::new(vec![
            Box::new(client_request_handler),
            Box::new(room_request_handler),
        ]);

        rouille::start_server(format!("{}:{}", &self.server, self.port), request_multi_handler);
    }
}

unsafe impl Send for ServerWrapperImpl {}

unsafe impl Sync for ServerWrapperImpl {}
