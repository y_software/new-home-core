use std::borrow::Cow;

use rouille::{Request, Response};

/// Defines an interface for handling interaction between the web server and the connected home-client(s)
pub trait RequestHandler {
    fn handle_request(&self, request: &Request) -> Response;
}

pub struct RequestMultiHandler {
    handlers: Vec<Box<dyn RequestHandler>>,
}

impl RequestMultiHandler {
    pub fn new(handlers: Vec<Box<dyn RequestHandler>>) -> Self {
        Self {
            handlers
        }
    }

    pub fn dispatch(&self, request: &Request) -> Response {
        for handler in &self.handlers {
            let response = handler.handle_request(request);

            if !response.is_error() && response.status_code != 404 {
                return response;
            }
        }

        if request.method() == "OPTIONS" {
            let mut response = Response::text("Ok");

            response.headers.push((
                Cow::from(String::from("Allow")),
                Cow::from(String::from("POST, PUT, GET, DELETE, CALL, HELP, OPTIONS"))
            ));

            return response;
        }

        Response::empty_404()
    }
}

impl FnOnce<(&Request, )> for RequestMultiHandler {
    type Output = Response;

    extern "rust-call" fn call_once(self, _args: (&Request, )) -> Self::Output {
        Response::empty_404()
    }
}

impl FnMut<(&Request, )> for RequestMultiHandler {
    extern "rust-call" fn call_mut(&mut self, _args: (&Request, )) -> Self::Output {
        Response::empty_404()
    }
}

impl Fn<(&Request, )> for RequestMultiHandler {
    /// Handles the roille request.
    /// This is directly called from the server.
    extern "rust-call" fn call(&self, args: (&Request, )) -> Self::Output {
        let request = args.0;
        let mut response = self.dispatch(request);

        response.headers.push((
            Cow::from(String::from("Access-Control-Allow-Origin")),
            Cow::from(String::from("*"))
        ));

        response.headers.push((
            Cow::from(String::from("Access-Control-Allow-Methods")),
            Cow::from(String::from("*"))
        ));

        response.headers.push((
            Cow::from(String::from("Access-Control-Allow-Headers")),
            Cow::from(String::from("*"))
        ));

        response
    }
}

unsafe impl Send for RequestMultiHandler {}

unsafe impl Sync for RequestMultiHandler {}
