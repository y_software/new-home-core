use serde::{Deserialize, Serialize};
use serde_json::Value;

use crate::communication::method_result::MethodResult;
use crate::communication::request::RequestData;
use crate::connection::connection_factory::ConnectionFactory;

#[derive(Serialize, Deserialize)]
pub struct ClientInfo {
    pub address: String,
    pub name: String,
}

impl ClientInfo {
    pub fn copy(own: &Self) -> Self {
        Self {
            address: own.address.clone(),
            name: own.name.clone(),
        }
    }
}

/// Contains the methods used to communicate with the (remote) application
pub trait Client {
    /// Gets the information how the client was registered
    fn get_info(&self) -> &ClientInfo;

    /// Calls a method on the remote application
    /// Will return the exact result of the method
    fn call_method(&self, method_name: &String, arguments: Value) -> MethodResult;
}

pub struct ClientImpl {
    client_info: ClientInfo,
    connect_factory: Box<dyn ConnectionFactory>,
}

impl ClientImpl {
    pub fn new(client_info: ClientInfo, connect_factory: Box<dyn ConnectionFactory>) -> Self {
        Self {
            client_info,
            connect_factory,
        }
    }
}

impl Client for ClientImpl {
    fn get_info(&self) -> &ClientInfo {
        &self.client_info
    }

    fn call_method(&self, method_name: &String, arguments: Value) -> MethodResult {
        let mut connection = match self.connect_factory.create_connection() {
            Err(_) => return MethodResult {
                code: 1,
                message: Some(json!([{
                    "error": "Could not connect to client"
                }])),
            },
            Ok(connection) => connection
        };

        let request = &RequestData {
            method_name: method_name.clone(),
            arguments,
        };

        connection.writer.write_line(&serde_json::to_string(&request).unwrap()).unwrap();

        let response = match connection.reader.read_line() {
            Err(_) =>
                return MethodResult {
                    code: 1,
                    message: Some(json!([{
                        "error": "Could not get response from client"
                    }])),
                },
            Ok(response) => response
        };

        let convert_result: serde_json::Result<MethodResult> = serde_json::from_str(&response);

        if convert_result.is_err() {
            return MethodResult {
                code: 1,
                message: Some(json!([{
                    "error": "Got invalid response from client"
                }])),
            };
        }

        convert_result.unwrap()
    }
}
