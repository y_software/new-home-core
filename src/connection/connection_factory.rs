use std::io::{BufReader, BufWriter};
use std::net::TcpStream;

use crate::net::{tcp_reader::TcpReader, tcp_writer::TcpWriter};
use crate::net::tcp_reader::TcpReaderImpl;
use crate::net::tcp_writer::TcpWriterImpl;
use std::io::Error as IoError;

/// Contains the connection reader and writer object
pub struct ClientConnection {
    pub reader: Box<dyn TcpReader>,

    pub writer: Box<dyn TcpWriter>,
}

/// Provides a function which returns a client connection consisting of a reader and a writer
pub trait ConnectionFactory {
    fn create_connection(&self) -> Result<ClientConnection, IoError>;
}

/// Implements the `ConnectionFactory` with the `TcpReader` and `TcpWriter` object(s)
pub struct ConnectionFactoryImpl {
    address: String
}

impl ConnectionFactoryImpl {
    pub fn new(address: String) -> Self {
        Self {
            address
        }
    }
}

impl ConnectionFactory for ConnectionFactoryImpl {
    fn create_connection(&self) -> Result<ClientConnection, IoError> {
        let stream = TcpStream::connect(self.address.clone())?;

        Ok(ClientConnection {
            reader: Box::new(TcpReaderImpl::new(BufReader::new(stream.try_clone().unwrap()))),
            writer: Box::new(TcpWriterImpl::new(BufWriter::new(stream))),
        })
    }
}
