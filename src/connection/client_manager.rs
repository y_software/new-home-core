use std::collections::HashMap;
use std::fs::OpenOptions;

use serde_json::Value;

use crate::communication::method_result::MethodResult;
use crate::connection::client::{Client, ClientImpl, ClientInfo};
use crate::connection::connection_factory::ConnectionFactoryImpl;

const CLIENTS_FILE: &str = "clients.yaml";

/// This trait represents the client manager as an interface
pub trait ClientManager {
    /// Stores a new client to the internal store with the given client info
    fn register_client(&mut self, address: String, name: String);

    /// Calls a method on a remote client
    /// Opens a TCP connection to the registered address if the client exists
    /// On error an errored `MethodResult` will be returned with the message content of the error description
    fn call_method(&self, client_name: &String, method_name: &String, arguments: Value) -> MethodResult;

    /// Returns the names of all registered clients
    fn get_registered_clients(&self) -> Vec<String>;

    /// Gets the reference to a given client if exists
    /// Otherwise `None` is returned
    fn get_client(&self, client_name: &String) -> Option<Box<dyn Client>>;

    /// Removes a client if it is registered
    /// Will error with an error message if client is not registered
    fn remove_client(&mut self, client_name: &String) -> Result<(), String>;
}

pub struct ClientManagerImpl {
    clients: HashMap<String, ClientInfo>,
}

impl ClientManagerImpl {
    pub fn new() -> Self {
        let mut manager = Self {
            clients: HashMap::new()
        };

        manager.read_yaml();

        manager
    }

    fn write_yaml(&self) {
        let file_result = OpenOptions::new().write(true).read(true).create(true).open(CLIENTS_FILE);

        if file_result.is_err() {
            println!("Could not open file {} for write due to an error: {:?}", CLIENTS_FILE, file_result.err().unwrap());

            return;
        }

        let clients_file = file_result.unwrap();

        let write_result = serde_yaml::to_writer(clients_file, &self.clients);

        if write_result.is_err() {
            println!("Could not write file {} due to an error: {:?}", CLIENTS_FILE, write_result.err().unwrap());
        }
    }

    fn read_yaml(&mut self) {
        let file_result = OpenOptions::new().read(true).open(CLIENTS_FILE);

        if file_result.is_err() {
            println!("Could not open file {} for read due to an error: {:?}", CLIENTS_FILE, file_result.err().unwrap());

            return;
        }

        let clients_file = file_result.unwrap();
        let from_reader_result = serde_yaml::from_reader(clients_file);

        if from_reader_result.is_err() {
            println!("Could not read clients from file {} due to an error: {:?}", CLIENTS_FILE, from_reader_result.err().unwrap());

            return;
        }

        let new_clients: HashMap<String, ClientInfo> = from_reader_result.unwrap();
        self.clients = new_clients;
    }
}

impl ClientManager for ClientManagerImpl {
    fn register_client(&mut self, address: String, name: String) {
        let client_info = ClientInfo {
            address,
            name: name.clone(),
        };

        self.clients.insert(
            name,
            client_info,
        );
        self.write_yaml();
    }

    fn call_method(&self, client_name: &String, method_name: &String, arguments: Value) -> MethodResult {
        let client_result = self.get_client(client_name);

        if client_result.is_none() {
            return MethodResult {
                code: 1,
                message: Some(Value::from("Client is not registered!")),
            };
        }

        client_result.unwrap().call_method(method_name, arguments)
    }

    fn get_registered_clients(&self) -> Vec<String> {
        let mut client_names = vec!();

        for key in self.clients.keys() {
            client_names.push(key.clone());
        }

        return client_names;
    }

    fn get_client(&self, client_name: &String) -> Option<Box<dyn Client>> {
        if !self.clients.contains_key(client_name) {
            return None;
        }

        let client_info = self.clients.get(client_name).unwrap();
        let factory = Box::new(ConnectionFactoryImpl::new(client_info.address.clone()));

        Some(Box::new(ClientImpl::new(
            ClientInfo::copy(client_info),
            factory,
        )))
    }

    fn remove_client(&mut self, client_name: &String) -> Result<(), String> {
        if !self.clients.contains_key(client_name) {
            return Err(String::from(format!("Client {} is not registered!", client_name)));
        }

        self.clients.remove(client_name);
        self.write_yaml();

        Ok(())
    }
}
