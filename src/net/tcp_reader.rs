use std::io::{BufRead, BufReader, Error};
use std::net::TcpStream;

/// Implements the `TcpReader` with a `BufReader` which is wrapping around a `std::net::TcpStream`
pub struct TcpReaderImpl {
    /// The reader which is called when the `read_line` method is called
    /// Is supplied in the constructor as already created `BufReader`
    reader: BufReader<TcpStream>
}

/// The `TcpReader` trait is meant to abstract some kind of a stream reader
/// This is useful in unit tests where a reader type is required
pub trait TcpReader {
    /// Reads a line from the connected `std::net::BufReader`
    /// Returns the read line
    fn read_line(&mut self) -> Result<String, Error>;
}

impl TcpReader for TcpReaderImpl {
    fn read_line(&mut self) -> Result<String, Error> {
        let mut buf = String::new();
        self.reader.read_line(&mut buf)?;

        Ok(buf)
    }
}

impl TcpReaderImpl {
    pub fn new(reader: BufReader<TcpStream>) -> Self {
        Self {
            reader
        }
    }
}
