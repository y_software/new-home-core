.PHONY: default
.PHONY: install
.PHONY: uninstall

default:
	@echo "You have to specify install or uninstall."

install:
	cargo check --all
	cargo build --release
	install systemd/new-home-core.service /usr/lib/systemd/system/new-home-core.service
	install target/release/new-home-core /usr/bin/new-home-core
	mkdir -p /etc/new-home-core

uninstall:
	rm /usr/lib/systemd/system/new-home-core.service
	rm /usr/bin/new-home-core
	rm -r /etc/new-home-core
