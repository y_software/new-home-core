[![Pipeline Status](https://gitlab.com/Y_Software/new-home-core/badges/master/pipeline.svg)](https://gitlab.com/Y_Software/new-home-core/pipelines)
[![creates.io](https://img.shields.io/crates/v/new-home-core.svg?style=flat-square&logo=rust)](https://crates.io/crates/new-home-core)

# New Home Core

This is the core application for the "new-home" project. The goal of the project is, to create a framework for controlling 
every single device in your home. Starting by your lights and electric sockets and going as far as controlling your home's
temperature. 

This part is only one many. It provides an API between the UI and the end devices. The other parts can be found here:
- [UI](https://gitlab.com/Y_Software/new-home-ui)
- [Application Framework](https://gitlab.com/Y_Software/new-home-application)

## Planed features

- Collecting all applications in your network
- Providing an HTTP API for maintaining all your devices
- Executing routines when things happen
- Respond to events sent by the registered applications

## Install and setup

### Install

**For the compilation you will need nightly Rust.**

To install the application you have to run the `(sudo) make install` command in the project folder as the root user.
This will install all necessary files where they are needed.

Configuration and resources of the application can be found in the `/etc/new-home-core` directory.

### Setup

After running the make install command, you can start the core via systemctl with the command `systemctl start new-home-core`.
Now the application should be reachable at *RASPBERRY_PI_IP*:5354. You can try this in your Browser. 

The next step is to set up the [new-home-ui](https://gitlab.com/Y_Software/new-home-ui).

### Uninstall

To uninstall this application you can just run `(sudo) make uninstall`. *This will remove the application and all its configuration permanently.*

## Documentation (scratch v0.1)

### Naming

The following table is a list of terms used to describe things in the context of this new-home project:

| Term | Meaning |
| --- | --- |
| Application | The application describes the new-home-application-Application which is running on the server (e.g. Raspberry PI) which handles method calls in the end. |
| Room | A room is used as a group which contains multiple devices (what in reality is basically a room at your home) |
| Device | The device is .. a device, for example a lamp. It is the device which gets controlled by the application. On one application there might be two lights connected to different GPIO pins. Each of those lamps is then a device. |
| Channel | A channel is displayed as a simple URL which describes where a device is reachable for the application. (e.g. Lamp 1 might be connected to GPIO pin 18, which could result in the following URL: `gpio://18` |
| View | The view is used in the frontend. It refers to the part in that users can do something. For example the Rooms or Applications overview |

### Required Methods

The following listed methods needs to be implemented in  the new-home-application in order to work as expected with the 
core or other new-home components:

| Method | Used in | Usage |
| --- | --- | --- |
| get_script | Core | This method is used to forward JavaScript file contents to the frontend. It gets a "script_name" as argument and has to respond with a "script" message which's content will be sent back with the content type "text/javascript". |
| config_root | Frontend | This method is used for displaying a configuration view in the frontend. It has to respond with an "action message" (more information about action messages can be found in the next section). |
| device_action | Frontend | This method is used for displaying a view in the frontend when the user clicks in the Rooms > Devices section on a device. This method as well has to return an action message. |

### Action messages

Action messages are used in the frontend to execute specific actions which can be defined in a simple JSON format.
For example the can cause a change of the current seen view in the frontend or open up a modal.

#### Action Types

*The definition of types can be found in the `script/functions/deviceActions/*.js` folder in the frontend.*

| Action | Description |
| --- | --- |
| error, message, markdown, success, debug, info | Those actions are used to display simple text messages in the frontend as a (small) reaction to something such as a button click. |
| scripts | In the scripts actions should be an array defined. It contains paths to other scripts, which are required for following actions to be executed (such as containing a custom component for the view or modal). |
| component | The component is used to set a component which will replace the current view. It contains the tag name of the upcoming HTML element which might be a custom HTML element. It will often be used with the "scripts" actions. |
| modal | This action shows a modal. It contains information about the appearance of the modal, such as a `title` the text for the `submit` button or the `close` button. While those three information are optional, the `component` info is required. It contains the tag name of the HTML element which should be loaded as the modal content. |

#### Structure

Action messages are built in JSON format. They can be written as one single object, or in multiple objects in an array.

```JSON
{
  "message": {
    "error": "Example error",
    "success": "You successfully did something"
  }
}
```

is equal to

```JSON
{
  "message": [
    {
      "error": "Example error"
    },
    {
      "success": "You successfully did something"
    }
  ]
}
```

Although both structures are handled the same way, it is recommended to use arrays to group actions. For example opening 
a view may contain `scripts` and `component` in one single object, because they are logically a group while an `info` 
message cause something different in the frontend besides the changed of the view.
