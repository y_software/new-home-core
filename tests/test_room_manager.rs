#[cfg(test)]
mod tests {
    use new_home_core::room::device::Device;
    use new_home_core::room::room_manager::{RoomManager, RoomManagerImpl};

    #[test]
    fn test_empty_get_rooms() {
        let manager = RoomManagerImpl::new(None);

        assert_eq!(manager.get_rooms().len(), 0)
    }

    #[test]
    fn test_add_room_ok() {
        let mut manager = RoomManagerImpl::new(None);

        assert!(manager.add_room(String::from("New Room")).is_ok());
        assert_eq!(manager.get_rooms().len(), 1)
    }

    #[test]
    fn test_add_room_err() {
        let mut manager = RoomManagerImpl::new(None);

        assert!(manager.add_room(String::from("New Room")).is_ok());
        assert!(manager.add_room(String::from("New Room")).is_err());
    }

    #[test]
    fn test_remove_room_ok() {
        let mut manager = RoomManagerImpl::new(None);

        assert!(manager.add_room(String::from("New Room")).is_ok());
        assert!(manager.remove_room(&String::from("New Room")).is_ok());
        assert_eq!(manager.get_rooms().len(), 0)
    }

    #[test]
    fn test_remove_room_err() {
        let mut manager = RoomManagerImpl::new(None);

        assert!(manager.remove_room(&String::from("New Room")).is_err());
    }

    #[test]
    fn test_missing_get_room_devices() {
        let manager = RoomManagerImpl::new(None);

        assert!(manager.get_room_devices(&String::from("Living Room")).is_err());
    }

    #[test]
    fn test_empty_get_room_devices() {
        let mut manager = RoomManagerImpl::new(None);

        assert!(manager.add_room(String::from("New Room")).is_ok());
        assert!(manager.get_room_devices(&String::from("New Room")).is_ok());
        assert_eq!(manager.get_room_devices(&String::from("New Room")).unwrap().len(), 0);
    }

    #[test]
    fn test_missing_add_room_device_err() {
        let mut manager = RoomManagerImpl::new(None);
        let device = Device { application: String::from("New Application"), channel: String::from("test:///unknown") };

        assert!(manager.add_room_device(String::from("New Room"), String::from("New Device"), device).is_err());
    }

    #[test]
    fn test_add_room_device_ok() {
        let mut manager = RoomManagerImpl::new(None);
        let device = Device { application: String::from("New Application"), channel: String::from("test:///unknown") };

        assert!(manager.add_room(String::from("New Room")).is_ok());
        assert!(manager.add_room_device(String::from("New Room"), String::from("New Device"), device).is_ok());
        assert_eq!(manager.get_room_devices(&String::from("New Room")).unwrap().len(), 1);
    }

    #[test]
    fn test_add_room_device_twice() {
        let mut manager = RoomManagerImpl::new(None);
        let device = Device { application: String::from("New Application"), channel: String::from("test:///unknown") };
        let device2 = Device { application: String::from("New Application"), channel: String::from("test:///unknown") };

        assert!(manager.add_room(String::from("New Room")).is_ok());
        assert!(manager.add_room_device(String::from("New Room"), String::from("New Device"), device).is_ok());
        assert!(manager.add_room_device(String::from("New Room"), String::from("New Device"), device2).is_err());
    }

    #[test]
    fn test_remove_room_device_ok() {
        let mut manager = RoomManagerImpl::new(None);
        let device = Device { application: String::from("New Application"), channel: String::from("test:///unknown") };

        assert!(manager.add_room(String::from("New Room")).is_ok());
        assert!(manager.add_room_device(String::from("New Room"), String::from("New Device"), device).is_ok());
        assert!(manager.remove_room_device(&String::from("New Room"), &String::from("New Device")).is_ok());
    }

    #[test]
    fn test_remove_room_device_missing_room() {
        let mut manager = RoomManagerImpl::new(None);

        assert!(manager.remove_room_device(&String::from("New Room"), &String::from("New Device")).is_err());
    }

    #[test]
    fn test_remove_room_device_missing_device() {
        let mut manager = RoomManagerImpl::new(None);

        assert!(manager.add_room(String::from("New Room")).is_ok());
        assert!(manager.remove_room_device(&String::from("New Room"), &String::from("New Device")).is_err());
    }
}
